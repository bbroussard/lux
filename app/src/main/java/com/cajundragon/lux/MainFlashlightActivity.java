package com.cajundragon.lux;

import android.content.Context;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.os.SystemClock.sleep;

public class MainFlashlightActivity extends AppCompatActivity implements Flashlight {

    private CameraManager mCameraManager;
    private String mCameraId;
    private Boolean isTorchOn;
    private Boolean isStrobeRunning;
    private EditText flashFrequencyField;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d("MainFlashlightActivity", "onCreate()");
        setContentView(R.layout.activity_main_flashlight);

        Button mTorchToggleButton = findViewById(R.id.torch_button);
        Button mTorchStrobeButton = findViewById(R.id.strobe_button);
        isTorchOn = false;
        isStrobeRunning = false;

        mCameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                mCameraId = mCameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }

        flashFrequencyField = findViewById(R.id.flash_frequency);

        mTorchToggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (isTorchOn) {
                        toggleTorch(v);
                        isTorchOn = false;
                        isStrobeRunning = false;
                    } else {
                        toggleTorch(v);
                        isTorchOn = true;
                    }
                } catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        mTorchStrobeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (isTorchOn) {
                        mCameraManager.setTorchMode(mCameraId, false);
                        isTorchOn = false;
                    }
                    strobeTorch(v);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void toggleTorch(View view) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!isTorchOn) {
                    mCameraManager.setTorchMode(mCameraId, true);
                } else {
                    mCameraManager.setTorchMode(mCameraId, false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void strobeTorch(View view) {
        int flashFrequency = Integer.valueOf(String.valueOf(flashFrequencyField.getText()));
        flashFrequency = (1000 / flashFrequency);
        try {
            if (isTorchOn) {
                mCameraManager.setTorchMode(mCameraId, false);
            } else {
                isTorchOn = true;
                isStrobeRunning = false;
            }

            isStrobeRunning = true;

            final int finalFlashFrequency = flashFrequency;
            Thread strobeThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    while (isStrobeRunning) {
                        try {
                            mCameraManager.setTorchMode(mCameraId, true);
                            sleep(finalFlashFrequency / 2);
                            mCameraManager.setTorchMode(mCameraId, false);
                            sleep(finalFlashFrequency / 2);
                        } catch (CameraAccessException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });

            Log.d("strobeTorch", "runnableFlash is starting...");
            strobeThread.start();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void toggleSOS() {
        //TODO: Provide logic similar to body of strobeTorch
    }
}

package com.cajundragon.lux;

import android.view.View;

interface Flashlight {
    void toggleTorch(View view);
    void strobeTorch(View view);
    void toggleSOS();
}
